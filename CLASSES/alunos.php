<?php
class Aluno {
    
    private $pdo;
    public $msgErro = "";
    public function conectar($nome, $host, $usuario, $senha) { // fazer a conexão ao banco de dados
        global $pdo;
        try {
            $pdo = new PDO("mysql:dbname=".$nome.";host=".$host, $usuario, $senha);
        } catch(PDOException $e) {
            $msgErro = $e->getMessage();
        }
        
    }
    
    public function cadastrar($nome, $aniversario) {
        global $pdo;
        // verificar se já existe email cadastrado
        $sql = $pdo->prepare("SELECT id_aluno FROM alunos WHERE nome = :n");
        $sql->bindvalue(":n", $nome);
        $sql->execute();
        if($sql->rowCount() > 0) {
            return false;
        } else {
            // caso contrário, cadastrar
            $sql = $pdo->prepare("INSERT INTO alunos (nome, data_nasc) VALUES (:n, :a)");
            $sql->bindValue(":n", $nome);
            $sql->bindValue(":a", $aniversario);
            $sql->execute();
            return true;
        }
        
    }
        
}
?>